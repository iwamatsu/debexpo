.. _contributing:

============
Contributing
============

Help is always welcome in debexpo. There are also a number of ways
to contribute:

* `Fixing bugs <https://salsa.debian.org/mentors.debian.net-team/debexpo/issues/>`_
* Testing the software and filing bugs
* Filing wishlist bugs
* Implementing new features
* Writing new plugins

Getting the source
==================

You can clone the Git repository using::

    git clone https://salsa.debian.org/mentors.debian.net-team/debexpo.git

Where to send patches
=====================

You should send patches either as `merge request <https://salsa.debian.org/mentors.debian.net-team/debexpo/merge_requests>`_
(preferred) or directly via email to us.

Send any other feedback and information reuqest to support@mentors.debian.net.

We also welcome Git branches.
